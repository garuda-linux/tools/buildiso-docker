#!/bin/bash
mknod /dev/loop0 b 7 0
[ -f /root/.ssh/id_ed25519 ] && chmod 600 /root/.ssh/id_ed25519
[ -z "$(ls -A /var/cache/garuda-tools/iso-profiles)" ] && buildiso -i

if [ "$1" == "auto" ]; then
  exec /usr/sbin/crond -n
elif [ "$1" == "auto-noweekly" ]; then
  rm -r /etc/cron.weekly
  exec /usr/sbin/crond -n
else
  exec "$@"
fi
