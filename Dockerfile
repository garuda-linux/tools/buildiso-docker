FROM docker.io/library/archlinux:latest

RUN pacman-key --init && \
	pacman-key --recv-key 0706B90D37D9B881 3056513887B78AEB --keyserver keyserver.ubuntu.com && \
	pacman-key --lsign-key 0706B90D37D9B881 3056513887B78AEB && \
	pacman --noconfirm -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-'{keyring,mirrorlist}'.pkg.tar.zst' && \
	echo "[multilib]" >>/etc/pacman.conf && \
	echo "Include = /etc/pacman.d/mirrorlist" >>/etc/pacman.conf && \
	echo -e "[garuda]\\nInclude = /etc/pacman.d/chaotic-mirrorlist\\n[chaotic-aur]\\nInclude = /etc/pacman.d/chaotic-mirrorlist" >>/etc/pacman.conf && \
	echo "" >>/etc/pacman.conf && \
	pacman -Syu --noconfirm apprise garuda-tools-iso-git garuda-hooks btrfs-progs lsb-release wget cronie && \
	pacman -Scc --noconfirm

COPY src/ /

ENTRYPOINT ["/entry_point.sh"]
